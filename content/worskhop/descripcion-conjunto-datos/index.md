---
title: 'Descripción Conjunto datos'
date: 2019-02-11T19:27:37+10:00
weight: 3
---


# De donde viene
El conjunto de datos de ejemplo contiene datos ofuscados de Google Analytics 360 de la tienda de Mercancias de Google, una tienda electrónica real. La Tienda de Google vende mercancía con la marca Google. Los datos son típicos de una tienda de su tipo. Esta incluye la siguiente información:

* **Datos de tráfico de origen:** Información que los visitantes al sitio web originan. Esta información incluye datos sobre tráfico orgánico, tráfico pago, etc.
* **Datos de contenido:** Información acerca del comportamiento de los usuarios en el sitio. Este incluye las URLs de las páginas que los visitantes ven, como interactúan con el contenido, etc.
* **Datos transaccionales:** Información acerca de las transacciones que ocurren en el sitio de la tienda de Google.

# Maneras de usar los datos
Ya que el conjunto de datos que provee Google Analytics 360 desde el sitio de comercio electrónico, éste es útil para explorar los beneficios de exportar los datos hacia Big Query. Una vez usted tenga acceso al conjunto de datos, puede ejecutar consultas como las que hay en la guía por el periodo de Agosto-2016 1 a Agosto-2017. Por ejemplo para ver el total de vistas (`pageviews`) que el sitio recibió para Enero 1-2017, usted consultaría el conjunto de datos con:

`SELECT SUM(totals.pageviews) as TotalPageviews
FROM [bigquery-public-data:google_analytics_sample.ga_sessions_20170101]`

# Limitaciones
Todos los usuarios tienen acceso de lectura al conjunto de datos. Esto significa que lo puede consultar y generar reportes pero no puede completar tareas administrativas. Datos para algunos campos han sido ofuscados como el Id del visitante (`fullVisitorId`) o removidos, como el Id del cliente (`clientId`, `adWordsClickInfo`, `geoNetwork`).
El valor `Not available in demo dataset` (No disponible en la fuente de datos demo) se mostrará para los valores alfanuméricos (STRING) y `null` para valores numéricos enteros (INTEGER) que no tengan datos.

# Conjunto de Datos y Tablas

`data-to-insights:ecommerce`

## Tabla all_sessions_raw 

Nombre Campo	Tipo	Modo	Descripción

fullVisitorId	STRING	NULLABLE	

channelGrouping	STRING	NULLABLE	

time	INTEGER	NULLABLE	

country	STRING	NULLABLE	

city	STRING	NULLABLE	

totalTransactionRevenue	INTEGER	NULLABLE	

transactions	INTEGER	NULLABLE	

timeOnSite	INTEGER	NULLABLE	

pageviews	INTEGER	NULLABLE	

sessionQualityDim	INTEGER	NULLABLE	

date	STRING	NULLABLE	

visitId	INTEGER	NULLABLE	

type	STRING	NULLABLE	

productRefundAmount	INTEGER	NULLABLE	

productQuantity	INTEGER	NULLABLE	

productPrice	INTEGER	NULLABLE	

productRevenue	INTEGER	NULLABLE	

productSKU	STRING	NULLABLE	

v2ProductName	STRING	NULLABLE	

v2ProductCategory	STRING	NULLABLE	

productVariant	STRING	NULLABLE	

currencyCode	STRING	NULLABLE	

itemQuantity	INTEGER	NULLABLE	

itemRevenue	INTEGER	NULLABLE	

transactionRevenue	INTEGER	NULLABLE	

transactionId	STRING	NULLABLE	

pageTitle	STRING	NULLABLE	

searchKeyword	STRING	NULLABLE	

pagePathLevel1	STRING	NULLABLE	

eCommerceAction_type	STRING	NULLABLE	

eCommerceAction_step	INTEGER	NULLABLE	

eCommerceAction_option	STRING	NULLABLE	


## categories

Nombre Campo	Tipo	Modo	Descripción

productSKU	STRING	NULLABLE	

category	STRING	NULLABLE


## product_list

Nombre Campo	Tipo	Modo	Descripción

productSKU	STRING	NULLABLE	

v2ProductName	STRING	NULLABLE	


## products

Nombre Campo	Tipo	Modo	Descripción

SKU	STRING	NULLABLE	

name	STRING	NULLABLE	

orderedQuantity	INTEGER	NULLABLE	

stockLevel	INTEGER	NULLABLE	

restockingLeadTime	INTEGER	NULLABLE	

sentimentScore	FLOAT	NULLABLE	

sentimentMagnitude	FLOAT	NULLABLE	


## sales_by_sku

Nombre Campo	Tipo	Modo	Descripción

productSKU	STRING	NULLABLE	

total_ordered	INTEGER	NULLABLE


## sales_report

Nombre Campo	Tipo	Modo	Descripción

productSKU	STRING	NULLABLE	

total_ordered	INTEGER	NULLABLE	

name	STRING	NULLABLE	

stockLevel	INTEGER	NULLABLE	

restockingLeadTime	INTEGER	NULLABLE	

sentimentScore	FLOAT	NULLABLE	

sentimentMagnitude	FLOAT	NULLABLE	

ratio	FLOAT	NULLABLE	