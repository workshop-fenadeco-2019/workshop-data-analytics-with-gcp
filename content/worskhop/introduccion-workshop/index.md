---
title: 'Introduccion Workshop'
date: 2019-02-11T19:27:37+10:00
draft: false
weight: 1
---

1. [Presentacion](https://workshop-fenadeco-2019.gitlab.io/workshop-data-analytics-with-gcp/presentacion-fenadeco-2019-globant-workshop.pdf) - _**Introduccion a proyectos usando datos - De concepto a producto**_(20 minutos o menos)
1. Introduccion a GCP y cloud computing (3 minutos)
1. Description del conjunto de datos (5 minutos)
1. Introduccion al GCP Big Query (5 minutos)
    1. Que es un tabla en big Query
    1. Como crear un tabla en Big query
1. Trabajando con BigQuery (30 minutos)
    1. Detectar duplicados
    1. Detectar registros con data faltante
    1. Calcular algunas metricas de interes
1. Introduction al DataStudio (3 minutos)
1. Crear 2 reportes con data studio (10 minutos)
    1. Definiendo las preguntas de negocio que se desean responder
    1. Creando los tableros de mando
1. Preguntas y practica libre (30 minutos)