---
title: 'Usando GCP BigQuery'
date: 2019-02-11T19:27:37+10:00
weight: 5
---
# Usando Google Cloud Platform BigQuery

Laboratorio de Referencia [Exploring Your Ecommerce Dataset with SQL in Google BigQuery](https://www.qwiklabs.com/focuses/3618?parent=catalog)

Bonus track [Introduction to SQL for BigQuery and Cloud SQL](https://www.qwiklabs.com/focuses/2802?parent=catalog)

## Introducción al BigQuery

1. Que el  [GCP BigQuery](https://cloud.google.com/bigquery/)

    *BigQuery, el almacén de datos empresarial, altamente escalable y sin servidores de Google, está diseñado para permitir que los analistas de datos sean más productivos, y tiene una relación de precio y rendimiento inigualable. Debido a que no se debe administrar ninguna infraestructura, puedes enfocarte en analizar los datos para obtener información valiosa con el lenguaje SQL con el que estás familiarizado, sin necesitar a un administrador de base de datos.*

1. Entendiendo la interfaz

    ![bigQuery_1](/workshop-data-analytics-with-gcp/img/bigQuery_1.png)

    1. Menu desplegable para Base de Datos `ecommerce`. Este menu lista todas las tablas disponibles en esta Base de Datos

    1. Listado de tablas

    1. Espacio de trabajo para Escribir y Ejectuar comandos SQL

    1. Resultados despues de ejecutar los comandos SQL

## Antes de empezar

1. Accede al link del laboratorio guia [Exploring Your Ecommerce Dataset with SQL in Google BigQuery](https://www.qwiklabs.com/focuses/3618?parent=catalog)

1. Ingresa a la consola GCP con las credenciales del laboratorio

    ![login_into_lab_qwiklab](/workshop-data-analytics-with-gcp/img/login_into_lab_qwiklab.gif)

1. Asegurate de usar la cuenta de qwiklabs para poder user GCP BigQuery

1. Debes activar el conjunto de datos. Abre este link [https://console.cloud.google.com/bigquery?p=data-to-insights&page=ecommerce](https://console.cloud.google.com/bigquery?p=data-to-insights&page=ecommerce )

    ![activating_ecommerce_dataset](/workshop-data-analytics-with-gcp/img/activating_ecommerce_dataset.gif)

## BigQuery aplicado en el mundo real

A continuación veremos como usando BigQuery junto con SQL podemos responder preguntas usando un conjunto de datos:


1. Una de las primeras tareas de gran importancia al utilizar un conjunto de datos para Inteligencia de Negocios, es aplicar [validacion de calidad de datos](https://es.wikipedia.org/wiki/Calidad_de_datos). En el SQL a mostrado a continuación *Detectaremos los registros duplicados*.
    
    ```sql
    #standardSQL
    SELECT COUNT(*) as num_duplicate_rows
        , * 
    FROM `data-to-insights.ecommerce.all_sessions_raw`
    GROUP BY
        fullVisitorId
        , channelGrouping
        , time
        , country
        , city
        , totalTransactionRevenue
        , transactions
        , timeOnSite
        , pageviews
        , sessionQualityDim
        , date
        , visitId
        , type
        , productRefundAmount
        , productQuantity
        , productPrice
        , productRevenue
        , productSKU
        , v2ProductName
        , v2ProductCategory
        , productVariant
        , currencyCode
        , itemQuantity
        , itemRevenue
        , transactionRevenue
        , transactionId
        , pageTitle
        , searchKeyword
        , pagePathLevel1
        , eCommerceAction_type
        , eCommerceAction_step
        , eCommerceAction_option
        HAVING num_duplicate_rows > 1;
    ```

    ![bigquery_1](/workshop-data-analytics-with-gcp/img/bigQuery_1.png)


    **IMPORTANTE:** Despues de haber detectado los duplicados deseamos obtener un conjunto de DATOS LIMPIO. Es por esto que para los ejercicios a continuacion usaremos la tabla `ecommerce.all_sessions` la cual **HA DESCARTADO LOS REGISTROS DUPLICADOS**. Se recomienda que esta tarea sea apoyo por Ingeniero de Datos o Programador.

1. Otra tarea de gran importancia, es la detección de valores faltantes en el conjunto de datos. Debido a la gran cantidad de columnas, se recomienda concentrarse únicamente en los valores que serán utilizados en el corto plazo o tienen un gran valor de información para la organización.
    
    Líder:¿Cuál es la calidad del conjunto de datos que se tiene?

    Analista: Como analistas de datos, le hemos respondido a nuestro líder que debido al gran tamaño del conjunto de datos nos ayudaria saber especificamente que respuestas espera obtener de estos datos

    Lider: El lider del area responde que el espera que solo se use la Tabla llamada `all_sessions_raw`
    
    Analista: Como primer paso, el analista va a aplicar un SELECT COUNT básico para detectar cuántos registros tienen nulos (valores faltantes) en las columnas de interés.

    ```sql
    SELECT searchKeyword, count(*) as count_
    FROM `data-to-insights.ecommerce.all_sessions`
    GROUP BY searchKeyword;
    ```

    ![bigquery_2](/workshop-data-analytics-with-gcp/img/bigQuery_2.png)

1. Tu lider de proyecto te ha pedido que identifiques  cuales son los paises con mayor número de visitas.

    ```sql
    SELECT country, count(*) as count_
    FROM `data-to-insights.ecommerce.all_sessions`
    GROUP BY country
    order by count_ DESC;
    ```

    ![bigquery_3](/workshop-data-analytics-with-gcp/img/bigQuery_3.png)

1. Tu lider de proyecto, desea saber cuáles países acceden más a la pagina y cual de estos tiene el mayor número de visitas además desea saber cuanto tiempo duran en ella. Esta información se podrá utilizar posteriormente para calcular tasas de conversión ( es decir, usuarios que acceden vs usuarios que compran) Ver interacciones 

    ```sql
    SELECT v2ProductCategory
        ,channelGrouping
        , country
        , searchKeyword
        , sum(pageviews) as Visitas
        , max(timeOnSite) as max_on_site
        , min(timeOnSite) as min_on_site
    FROM `data-to-insights.ecommerce.all_sessions` 
    WHERE timeOnSite IS NOT NULL
        AND type='EVENT'
    GROUP BY v2ProductCategory
        , channelGrouping
        , country
        , searchKeyword
    ORDER BY Visitas DESC;
    ```

    ![bigquery_4](/workshop-data-analytics-with-gcp/img/bigQuery_4.png)

# referencias

* [BigQuery - Referencia SQL](https://cloud.google.com/bigquery/docs/reference/standard-sql/functions-and-operators#top_of_page)

* [Otros curso complementarios usando BigQuery](https://www.qwiklabs.com/quests/70?catalog_rank=%7B%22rank%22%3A4%2C%22num_filters%22%3A0%2C%22has_search%22%3Atrue%7D&search_id=3572322)

