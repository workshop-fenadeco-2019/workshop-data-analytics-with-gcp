---
title: 'Acerca de los Autores'
date: 2018-11-28T15:14:39+10:00
weight: 9
---

# Autores

* [Sebastian Suarez Benjumea](https://co.linkedin.com/in/ssuarezbe)

<img src="/workshop-data-analytics-with-gcp/img/sebas_foto.png" width="150" height="150">

* [Oscar Lopez](https://www.linkedin.com/in/oscar-lopez-cardona/)


<img src="/workshop-data-analytics-with-gcp/img/oscar_foto.png" width="150" height="150">

# Agradecimientos


* [Andres Tenorio](https://co.linkedin.com/in/andrestenorio)

* [Iván Dario Trebilcock Caviedes](https://co.linkedin.com/in/iv%C3%A1n-dario-trebilcock-caviedes-087b09a1)

* [Globant](https://www.globant.com/studio/big-data)

* [Comite Organizador FENALDECO 2019](http://fenadeco.org/congreso2019/)
