---
title: 'Enriqueciendo los datos'
date: 2019-02-11T19:27:37+10:00
weight: 7
---

# Enriquenciendo un conjuntos de Datos usando Fuentes Externas

Hay situacines donde la informacion presente en un conjunto de datos no es suficiente. En estas casos siempre existe la opcion de consultar Fuentes Externas de datos e integrar la informacion. El proceso de integracion puede variar en su nivel de dificultad, pero es una tarea que Ingeniero de Datos (Data Engineer) esta capacitado para apoyar.

Para el caso especifico del conjunto de datos `data-to-insights:ecommerce`, podemos usar otras herramientas ofrecidas por GCP.

Dentro de la oferta de herramientas de IA (Inteligencia Artificial), GCP ofrece [procesamiento de lenguaje natural.](https://cloud.google.com/natural-language/
)

![nl_gcp_example](/workshop-data-analytics-with-gcp/img/nl_gcp_example.gif)


Como se puede observar, GCP ofrece gran facilidad para aplicar ANALISIS DE SENTIMIENTOS SOBRE TEXTO. Esta herramientas puede ser usada para analizar **opiniones de productos** o **comentarios** y ayudar a tener un mejor perspectiva del negocio.

Un ejemplo de como se puede ver estos datos ANALISIS DE SENTIMIENTOS SOBRE TEXTO **integrados en BigQuery** se puede ver la tabla `ecommerce.sales_report`


![sentiment_data_bigquery](/workshop-data-analytics-with-gcp/img/sentiment_data_bigquery.png)


# Referencias

* [Intro to ML: Language Processing](https://www.qwiklabs.com/quests/82?catalog_rank=%7B%22rank%22%3A1%2C%22num_filters%22%3A0%2C%22has_search%22%3Atrue%7D&search_id=3598960)

* [Using the Natural Language API from Google Docs](https://www.qwiklabs.com/focuses/680?catalog_rank=%7B%22rank%22%3A5%2C%22num_filters%22%3A0%2C%22has_search%22%3Atrue%7D&parent=catalog&search_id=3598960)

* [Creating a Data Warehouse Through Joins and Unions](https://www.qwiklabs.com/focuses/3640?parent=catalog)