---
title: 'Introduccion al Standard Query Language (SQL)'
date: 2019-02-11T19:27:37+10:00
weight: 4
---

# Comandos Standard Query Language

## [SELECT](https://www.quackit.com/sql/tutorial/sql_select.cfm)

* Dada la **TABLA** `tabla_usuarios` con los siguientes registros:

    | Id | Nombre | Apellido  |   UserName  |
    |----|--------|-----------|:-----------:|
    | 1  | Fred   | Flinstone | freddo      |
    | 2  | Homer  | Simpson   | homey       |
    | 3  | Homer  | Brown     | notsofamous |
    | 4  | Ozzy   | Ozzbourne | sabbath     |
    | 5  | Homer  | Gain      | noplacelike |

* Si deseamos SELECCIONAR unicamente las columnas `Id, Apellido, UserName` la busqueda SQL es:

    ```sql
    SELECT 
    Id, Apellido, UserName
    FROM
    tabla_usuarios
    ```


    | Id | Apellido | UserName    |
    |----|----------|-------------|
    | 2  | Simpson  | homey       |
    | 3  | Brown    | notsofamous |
    | 5  | Gain     | noplacelike |

## [WHERE](https://www.quackit.com/sql/tutorial/sql_where.cfm)


* Dada la **TABLA** `tabla_usuarios` con los siguientes registros:

    | Id | Nombre | Apellido  |   UserName  |
    |----|--------|-----------|:-----------:|
    | 1  | Fred   | Flinstone | freddo      |
    | 2  | Homer  | Simpson   | homey       |
    | 3  | Homer  | Brown     | notsofamous |
    | 4  | Ozzy   | Ozzbourne | sabbath     |
    | 5  | Homer  | Gain      | noplacelike |

* Si deseamos ENCONTRAR UNICAMNTE los registros que TIENEN `Nombre = 'Homer'` la busqueda SQL es:

    ```sql
    SELECT * FROM tabla_usuarios
    WHERE Nombre = 'Homer';
    ```

    | Id | Nombre | Apellido | UserName    |
    |----|--------|----------|-------------|
    | 2  | Homer  | Simpson  | homey       |
    | 3  | Homer  | Brown    | notsofamous |
    | 5  | Homer  | Gain     | noplacelike |


* Si deseamos ENCONTRAR UNICAMNTE los registros que TIENEN `Nombre = 'Homer'` Y `Apellido = 'Brown'` la busqueda SQL es:

    ```sql
    SELECT * FROM tabla_usuarios
    WHERE Nombre = 'Homer'
    AND Apellido = 'Brown';
    ```

    | Id | Nombre | Apellido | UserName    |
    |----|--------|----------|-------------|
    | 3  | Homer  | Brown    | notsofamous |

## [JOIN](https://www.tutorialrepublic.com/sql-tutorial/sql-inner-join-operation.php)

* Dada la **TABLA** `empleado` con los siguientes registros:

    | emp_id | emp_nombre   | emp_fecha_contr | id_area |
    |--------|--------------|-----------------|---------|
    |      1 | Ethan Hunt   |    2001-05-01   |       4 |
    |      2 | Tony Montana |    2002-07-15   |       1 |
    |      3 | Sarah Connor |    2005-10-18   |       5 |
    |      4 | Rick Deckard |    2007-01-03   |       3 |
    |      5 | Martin Blank |    2008-06-24   |    NULL |


* Dada la TABLA `area_organizacion` con los siguientes registros:
    
    | id_area | nombre_area     |
    |---------|------------------|
    |       1 | Administration   |
    |       2 | Customer Service |
    |       3 | Finance          |
    |       4 | Human Resources  |
    |       5 | Sales            |


* Si deseamos encontrar `ID del empleado, Nombre del Empleado, Fecha de Contratacion y Area a la cual pertenece` la busqueda SQL es:

    ```sql
    SELECT 
    t1.emp_id, 
    t1.emp_nombre, 
    t1.emp_fecha_contr, 
    t2.nombre_area
    FROM empleado AS t1 INNER JOIN area_organizacion AS t2
    ON t1.id_area = t2.id_area ORDER BY emp_id;
    ```


    | emp_id | emp_nombre   | emp_fecha_contr | nombre_area     |
    |--------|--------------|-----------------|-----------------|
    |      1 | Ethan Hunt   |    2001-05-01   | Human Resources |
    |      2 | Tony Montana |    2002-07-15   | Administration  |
    |      3 | Sarah Connor |    2005-10-18   | Sales           |
    |      4 | Rick Deckard |    2007-01-03   | Finance         |


## [GROUP BY](https://www.tutorialrepublic.com/sql-tutorial/sql-group-by-clause.php)

* Dada la **TABLA** `empleado` con los siguientes registros:

    | emp_id | emp_nombre   | emp_fecha_contr | id_area |
    |--------|--------------|-----------------|---------|
    |      1 | Ethan Hunt   |    2001-05-01   |       4 |
    |      2 | Tony Montana |    2002-07-15   |       1 |
    |      3 | Sarah Connor |    2005-10-18   |       5 |
    |      4 | Rick Deckard |    2007-01-03   |       3 |
    |      5 | Martin Blank |    2008-06-24   |    NULL |


* Dada la **TABLA** `area_organizacion` con los siguientes registros:
    
    | id_area | nombre_area     |
    |---------|------------------|
    |       1 | Administration   |
    |       2 | Customer Service |
    |       3 | Finance          |
    |       4 | Human Resources  |
    |       5 | Sales            |

* Se desea saber cuantos **empleados** hay en cada **area de la organizacion**:

    ```sql
    SELECT 
    t1.dept_name, 
    count(t2.emp_id) AS total_employees
    FROM 
    departments AS t1 
    LEFT JOIN employees AS t2
    ON t1.dept_id = t2.dept_id
    GROUP BY t1.dept_name;
    ```

    | dept_name         | total_employees |
    |-------------------|-----------------|
    | Administration    |               1 |
    | Customer Service  |               0 |
    | Finance           |               1 |
    | Human Resources   |               1 |
    | Sales             |               1 |

# Referencias

* https://www.quackit.com/sql/tutorial/sql_select.cfm

* https://www.quackit.com/sql/tutorial/sql_where.cfm

* https://www.tutorialrepublic.com/sql-tutorial/sql-inner-join-operation.php

* https://www.tutorialrepublic.com/sql-tutorial/sql-group-by-clause.php