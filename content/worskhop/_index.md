---
title: 'Sinopis'
date: 2018-11-28T15:14:39+10:00
weight: 1
---

## Workshop FENADECO 2019 - Analizando Datos con GCP

El objetivo de este workshop es introducir a los asistentes desde una vista de 5000 mt de altura el funcionamiento y las fases de un proyecto orientado a Analitica de Datos


**NOTA IMPORTANTE:** Cada participante del workshop debe tener una cuent activa en https://www.qwiklabs.com/ 