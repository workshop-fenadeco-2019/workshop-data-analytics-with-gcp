---
title: 'Usando GCP Data Studio'
date: 2019-02-11T19:27:37+10:00
weight: 6
---

# Usando Google Cloud Platform Data Studio

Laboratorio de Referencia [Explore and Create Reports with Data Studio](https://www.qwiklabs.com/focuses/3614?parent=catalog)



## Introduccion al Data Studio

1. Que es el [GCP Data Studio](https://cloud.google.com/data-studio/)

*Data Studio turns your data into informative, easy to read, easy to share, and fully customizable dashboards and reports.*


1. Entendiendo la interfaz



## Antes de empezar

1. Accede al link del laboratorio guia [Explore and Create Reports with Data Studio](https://www.qwiklabs.com/focuses/3614?parent=catalog)

1. Ingresa a la consola GCP con las credenciales del laboratorio

    ![login_into_lab_qwiklab](/workshop-data-analytics-with-gcp/img/login_into_lab_qwiklab.gif)

1. Asegurate de usar la cuenta de qwiklabs para pder user GCP BigQuery

1. Activa el [data studio](https://datastudio.google.com/)

    ![activate_data_studio](/workshop-data-analytics-with-gcp/img/activate_data_studio.gif)


## Crear reporte.


1. Bienvenida a Google data studio.

    * Damos clic en Get Started
    
    ![create_report_2.png](/workshop-data-analytics-with-gcp/img/create_report_2.png)

    * Aceptamos los terminos de servicio.
    
    ![create_report_3.png](/workshop-data-analytics-with-gcp/img/create_report_3.png)

    * Encuesta de preferencias, seleccionamos para todos los campos 'No, thanks' y damos clic en Done.
    
    ![create_report_4.png](/workshop-data-analytics-with-gcp/img/create_report_4.png)

1. Seleccionamos reporte en blanco (Blank Report)
    ![create_report.png](/workshop-data-analytics-with-gcp/img/create_report.png)

1. Agregar data Source(Agregar fuente de datos).
    
    ![select_data_source1.png](/workshop-data-analytics-with-gcp/img/select_data_source1.png)

    * Damos clic en +CREATE NEW DATA SOURCE y seleccionamos BigQuery
    
    ![select_data_source2.png](/workshop-data-analytics-with-gcp/img/select_data_source_2.png)

    * Damos clic en  AUTHORIZE Y luego en el siguiente pop-up damos click en ALLOW
    
    ![select_data_source3.png](/workshop-data-analytics-with-gcp/img/select_data_source_3.png)

    * Una vez estamos en el menu seleccionamos en la primera columna QUESTION QUERY y en la segunda seleccionamos quicklabs, una ves realizado este paso nos habilitara una TextBox en el cual pegaremos el siguiente query.
    
    ![select_data_source4.png](/workshop-data-analytics-with-gcp/img/select_data_source_4.png)

    ```sql
    SELECT v2ProductCategory, channelGrouping, country,
    sum(pageviews) as total_page_views, 
    max(timeOnSite) as max_time_on_sinte,  
    min(timeOnSite) as min_time_on_site,
    avg(timeOnSite) as avg_time_on_site 
    FROM 
    `data-to-insights.ecommerce.all_sessions_raw` 
    WHERE timeOnSite is not null 
    AND type='EVENT'
    GROUP BY 
    v2ProductCategory, 
    channelGrouping, 
    country;
    ```


1. Damos clic en connectar y pasaremos a la pantalla de del query, donde veremos reflejadas las columnas que proyectamos en nuestro query, una vez allí damos clic en ADD TO REPORT.

    ![add_report_1.png](/workshop-data-analytics-with-gcp/img/add_report_1.png)

![enable_datasource_data_studio.gif](/workshop-data-analytics-with-gcp/img/enable_datasource_data_studio.gif)

### Creando nuestro primer DashBoard.

1. Insertando filtros: Agregaremos dos filtros a nuestro DashBoard, uno por ciudad y otro por producto.

    ![filters.png](/workshop-data-analytics-with-gcp/img/filters.png)
    
    * Agregando filtro por ciudad, en dimensón seleccionamos country.
    
    ![filter_dimension_country.png](/workshop-data-analytics-with-gcp/img/filter_dimension_country.png)
    
    * Agregar filtro por categoría, se debe realizar la misma operación que ciudad, pero en dimensión se debe seleccionar v2ProductCategory.

1. Insertando tabla pivote.
    En la pestaña insert seleccionamos 'Pivot Table'.
    Configurando valores a mostrar
        Row dimension:
            v2ProductCategory
            channelGrouping

        Column dimension:
            country

        Metric:
            total_page_views
            max_time_on_sinte
            min_time_on_site
            avg_time_on_site

    ![table_pivote.png](/workshop-data-analytics-with-gcp/img/table_pivote.png)

    * Insertando mapa de geolocalización
        Nuevamente en el menu de insert, seleccionamos 'Geo Map'
        Configurando valores a mostrar
        Dimension:
            country
        Metrics:
            total_page_views

        ![geo_map.png](/workshop-data-analytics-with-gcp/img/geo_map.png)

    * Insertando Diagrama de barras
        Nuevamente en el menu de insert, seleccionamos 'Combo Chart'
        Dimension:
            v2ProductCategory
        Metrics:
            total_page_views
            avg_time_on_site
        Sort:
            total_page_views

        ![combo_chart.png](/workshop-data-analytics-with-gcp/img/combo_chart.png)




# Referencias

https://www.qwiklabs.com/focuses/3614?parent=catalog

https://www.qwiklabs.com/quests/70?catalog_rank=%7B%22rank%22%3A4%2C%22num_filters%22%3A0%2C%22has_search%22%3Atrue%7D&search_id=3572322