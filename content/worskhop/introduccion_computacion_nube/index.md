---
title: 'Introduccion a la computacion en la nube'
date: 2019-02-11T19:27:37+10:00
weight: 2
---

# Introducción a la computación en la nube

* El término "Nube" se refiere al Internet.
* El término "Computación en la nube" se refiere a la computación basada en Internet donde diferentes servicios, incluyendo servidores, almacenamiento y aplicaciones son enviados a los servidores y dispositivos de una compañía a traves del Internet. 

## Categorías de servicios en la nube

* **IaaS (Infrastructure-as-a-Service) (Infraestructura como servicio):** Maneja capacidad de computo. Ej. Instalar una red de máquinas para procesar datos.
* **PaaS (Platform-as-a-Service) (Plataforma como servicio):** 
Soporta creacion de software para sistemas computacionales que lo necesitan. Ej. Crear una aplicacion Web.
* **SAAS (Software-as-a-Service):** Este provee software. Ej. Gmail y Netflix

## Ventajas de la computación en la nube
* Reducción en costos de mantenimiento
* Escalabilidad
* Continuidad del negocio
* Eficiencia en la colaboración remota
* Flexibilidad de prácticas laborales
* Acceso a a actualizaciones automáticas

## Principales proveedores de computación en la nube
* AWS (Amazon Web Services)
* Google Cloud Platform (GCP)
* Microsoft Azure

# Links
https://www.dataversity.net/brief-history-cloud-computing/
https://www.webopedia.com/quick_ref/cloud_computing.asp
https://www.business.qld.gov.au/running-business/it/cloud-computing/benefits
https://aws.amazon.com/
https://cloud.google.com/
https://azure.microsoft.com
